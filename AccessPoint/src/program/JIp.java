package program;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.text.html.HTMLEditorKit.Parser;

import model.IFajlovi;
import model.JTextFieldLimit;
import model.Router;
import model.Uredjaj;

import javax.swing.JRadioButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.PixelInterleavedSampleModel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.SwingConstants;
import javax.swing.JCheckBox;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.SystemColor;

public class JIp implements IFajlovi {
	ArrayList<Uredjaj> listaUredjaja = new ArrayList<>();

	private JFrame frame;

	private String sifra; 						//SIFRA Wifi


	public String getSifra() {
		return sifra;
	}
	private JLabel lblNewLabel;
	private JLabel PC;
	private JLabel linija1;
	private JLabel Laptop;
	private JLabel linija2;
	private JButton btnLaptopEthernet;
	private JButton btnLaptopWifi;
	private JPasswordField passwordFieldWifi2;
	private JLabel labelWifi2;
	private JLabel A52016;
	private JTextField textFieldUsername;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private JPasswordField passwordField_2;
	private JButton btnEnter;
	private JLabel labelWifi1;
	private JLabel labelIspisIp;
	private JPasswordField passwordFieldWifi1;
	private JButton btnNewButton;
	private JLabel linija3;
	private JPasswordField passwordFieldWifi3;
	private JLabel labelWifi3;



	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JIp window = new JIp();
					window.frame.setVisible(true);


				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JIp() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				probaBrisanja("Adrese.txt");
				probaBrisanja("Pokazivac.txt");
				probaPisanja("Pokazivac.txt", "101");
			}
		});
		frame.setBounds(100, 100, 799, 617);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);



		JTextArea textArea = new JTextArea();
		textArea.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				textArea.setVisible(false);
			}
		});
		textArea.setBounds(153, 339, 400, 218);
		frame.getContentPane().add(textArea);
		textArea.setForeground(Color.WHITE);
		textArea.setBackground(Color.BLACK);
		textArea.setVisible(false);

		JLabel lblSetIpAdress = new JLabel(" Ip configuration>>");
		lblSetIpAdress.setFont(new Font("Tahoma", Font.ITALIC, 13));
		lblSetIpAdress.setForeground(Color.BLUE);
		lblSetIpAdress.setBackground(Color.WHITE);
		lblSetIpAdress.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				Router r = new Router("TP-link", "IP:192.168.0.1", "SubNET Mask: 255.255.255.0");
				labelIspisIp.setText(r.toString());





			}
		});
		lblSetIpAdress.setBounds(343, 292, 143, 16);
		frame.getContentPane().add(lblSetIpAdress);

		lblNewLabel = new JLabel("");
		lblNewLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				JOptionPane.showMessageDialog(null,	"BROJ KONEKTOVANIH UREDJAJA: "+listaUredjaja.size());
			}
		});
		lblNewLabel.setIcon(new ImageIcon("F:\\eclipse-workspace\\Router-PROJECT\\Router-SLIKE\\router.png"));
		lblNewLabel.setBounds(334, 141, 152, 152);
		frame.getContentPane().add(lblNewLabel);

		PC = new JLabel("PC-icon");
		PC.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(linija1.getForeground()==Color.green)
					JOptionPane.showMessageDialog(null, "Uredjaj uspesno konektovan!");
				else
					JOptionPane.showMessageDialog(null, "Nije uspostavljena konekcija!");
			}
		});
		PC.setVerticalAlignment(SwingConstants.TOP);
		PC.setIcon(new ImageIcon("F:\\eclipse-workspace\\Router-PROJECT\\Router-SLIKE\\desktop-racunar.png"));
		PC.setBounds(14, 60, 163, 97);
		frame.getContentPane().add(PC);

		linija1 = new JLabel("-----------");
		linija1.setFont(new Font("Tahoma", Font.BOLD, 31));
		linija1.setForeground(Color.RED);
		linija1.setBounds(189, 141, 163, 16);
		frame.getContentPane().add(linija1);

		Laptop = new JLabel("laptop-icon");
		Laptop.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(linija2.getForeground()==Color.green)
					JOptionPane.showMessageDialog(null, "Uredjaj uspesno konektovan!");
				else
					JOptionPane.showMessageDialog(null, "Nije uspostavljena konekcija!");
			}
		});
		Laptop.setIcon(new ImageIcon("F:\\eclipse-workspace\\Router-PROJECT\\Router-SLIKE\\laptop.png"));
		Laptop.setBounds(14, 292, 163, 103);
		frame.getContentPane().add(Laptop);

		linija2 = new JLabel("------------");
		linija2.setHorizontalAlignment(SwingConstants.CENTER);
		linija2.setForeground(Color.RED);
		linija2.setFont(new Font("Tahoma", Font.BOLD, 31));
		linija2.setBounds(128, 292, 167, 16);
		frame.getContentPane().add(linija2);



		btnLaptopWifi = new JButton("Wifi");
		btnLaptopWifi.setForeground(Color.BLUE);
		btnLaptopWifi.setIcon(new ImageIcon("F:\\eclipse-workspace\\Router-PROJECT\\Router-SLIKE\\wifi-icon.png"));
		btnLaptopEthernet = new JButton("Ethernet");
		btnLaptopEthernet.setForeground(Color.ORANGE);
		btnLaptopEthernet.setIcon(new ImageIcon("F:\\eclipse-workspace\\Router-PROJECT\\Router-SLIKE\\ethernet.png"));
		btnLaptopEthernet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				linija2.setForeground(Color.GREEN);


				JSetIp prvi = new JSetIp("Lenovo", "Ethernet");
				prvi.main(null);

				btnLaptopEthernet.setEnabled(false);








			}
		});
		btnLaptopEthernet.setBounds(16, 397, 136, 40);
		frame.getContentPane().add(btnLaptopEthernet);

		passwordFieldWifi2 = new JPasswordField();
		passwordFieldWifi2.setBounds(172, 265, 102, 25);
		frame.getContentPane().add(passwordFieldWifi2);
		passwordFieldWifi2.setVisible(false);

		labelWifi2 = new JLabel("Enter key:");
		labelWifi2.setBounds(14, 234, 186, 16);
		frame.getContentPane().add(labelWifi2);
		labelWifi2.setVisible(false);



		JPanel panel2 = new JPanel();
		panel2.setBorder(new TitledBorder(null, "Set password", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel2.setBounds(551, 328, 221, 242);
		frame.getContentPane().add(panel2);
		panel2.setLayout(null);

		JLabel lblNewLabel_3 = new JLabel("User_name:");
		lblNewLabel_3.setBounds(12, 31, 82, 16);
		panel2.add(lblNewLabel_3);

		textFieldUsername = new JTextField();
		textFieldUsername.setBounds(106, 28, 96, 22);
		panel2.add(textFieldUsername);
		textFieldUsername.setColumns(10);

		JLabel lblPassword = new JLabel("Old password:");
		lblPassword.setBounds(12, 60, 89, 16);
		panel2.add(lblPassword);

		passwordField = new JPasswordField();
		passwordField.setBounds(106, 63, 96, 22);
		panel2.add(passwordField);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(23, 123, 186, 2);
		panel2.add(separator_1);

		JLabel lblNewPassword = new JLabel("New password:");
		lblNewPassword.setBounds(12, 138, 89, 16);
		panel2.add(lblNewPassword);

		JLabel lblRepeatPassword = new JLabel("Repeat pass:");
		lblRepeatPassword.setBounds(12, 173, 104, 16);
		panel2.add(lblRepeatPassword);

		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(113, 135, 96, 22);
		panel2.add(passwordField_1);

		passwordField_2 = new JPasswordField();
		passwordField_2.setBounds(113, 170, 96, 22);
		panel2.add(passwordField_2);

		JButton btnChangePassword = new JButton("CHANGE");
		btnChangePassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String newPassword=passwordField_1.getText().trim();
				String password=passwordField_2.getText().trim();
				if(newPassword!=null && newPassword.length()>0 && password!= null && password.length()>0) {

					if(newPassword.equals(password)) {
						panel2.setVisible(false);
						//USLOV-KONEKCIJA
						JOptionPane.showMessageDialog(null, "Sifra uspesno promenjena");
						sifra=newPassword;

					}
					else {
						JOptionPane.showMessageDialog(null, "Pokusajte ponovo!");
						passwordField_1.setText("");
						passwordField_2.setText("");
					}


				}
				else {
					JOptionPane.showMessageDialog(null, "Morate popuniti sva polja!");
				}
			}
		});
		btnChangePassword.setForeground(Color.WHITE);
		btnChangePassword.setBackground(Color.BLUE);
		btnChangePassword.setBounds(40, 204, 134, 25);
		panel2.add(btnChangePassword);
		panel2.setVisible(false);

		btnEnter = new JButton("ENTER");
		btnEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String userName=textFieldUsername.getText().trim();
				String password=passwordField.getText().trim();
				if(userName!=null && userName.length()>0 && password!= null && password.length()>0) {

					if(userName.equals("admin") && password.equals("test")) {
						separator_1.setVisible(true);
						lblNewPassword.setVisible(true);
						lblRepeatPassword.setVisible(true);
						passwordField_1.setVisible(true);
						passwordField_2.setVisible(true);
						btnChangePassword.setVisible(true);
						JOptionPane.showMessageDialog(null, "USPESNO");
					}
					else {
						JOptionPane.showMessageDialog(null, "Pogresno uneto Username ili Password");
						textFieldUsername.setText("");
						passwordField.setText("");
					}


				}
				else {
					JOptionPane.showMessageDialog(null, "Morate popuniti sva polja!");
				}
			}

		});
		btnEnter.setBackground(Color.BLUE);
		btnEnter.setForeground(Color.WHITE);
		btnEnter.setBounds(40, 89, 134, 25);
		panel2.add(btnEnter);



		JLabel labelaWifi2 = new JLabel("");
		labelaWifi2.setIcon(new ImageIcon("F:\\eclipse-workspace\\Router-PROJECTbeta\\Router-SLIKE\\wi-fi-connection-issues.png"));
		labelaWifi2.setBounds(321, 11, 180, 146);
		frame.getContentPane().add(labelaWifi2);
		labelaWifi2.setVisible(false);

		JCheckBox checkBoxWifi = new JCheckBox("Enable Wifi LAN");
		checkBoxWifi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(checkBoxWifi.isSelected()==true) {
					labelaWifi2.setVisible(true);
				}
					else 
					if(checkBoxWifi.isSelected()==false){
						labelaWifi2.setVisible(false);
						
					}
				}
			
		});
		checkBoxWifi.setBounds(343, 118, 129, 25);
		frame.getContentPane().add(checkBoxWifi);

		//BUTTON WIfi
		btnLaptopWifi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if(checkBoxWifi.isSelected()==true) {
					passwordFieldWifi2.setVisible(true);
					btnLaptopWifi.setText("Connect");
					labelWifi2.setVisible(true);
					labelWifi2.setText("Please enter the security key");

					if(passwordFieldWifi2.getText().length()>0 && passwordFieldWifi2.getText()!=null){
						if(passwordFieldWifi2.getText().equals(sifra) || passwordFieldWifi2.getText().equals("test")) {
							linija2.setForeground(Color.GREEN);
							JOptionPane.showMessageDialog(null, "Uspesna konekcija!");
							btnLaptopWifi.setText("Connected!");
							passwordFieldWifi2.setVisible(false);
							labelWifi2.setVisible(false);


							Uredjaj u = new Uredjaj("Lenovo", "wifi");

							JSetIp drugi = new JSetIp(u.getNaziv(), "Wifi");
							drugi.main(null);



							btnLaptopWifi.setEnabled(false);








						}
						else {
							JOptionPane.showMessageDialog(null, "Pogresna sifra!");
							passwordFieldWifi2.setText("");
						}
					}
					else {
						//JOptionPane.showMessageDialog(null, "Morate popuniti sva polja!");
					}

				}


				else {
					JOptionPane.showMessageDialog(null, "Nema dostupnih Wifi!");
				}




			}
		});
		btnLaptopWifi.setBounds(16, 252, 136, 40);
		frame.getContentPane().add(btnLaptopWifi);

		A52016 = new JLabel("");
		A52016.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(linija3.getForeground()==Color.green)
					JOptionPane.showMessageDialog(null, "Uredjaj uspesno konektovan!");
				else
					JOptionPane.showMessageDialog(null, "Nije uspostavljena konekcija!");
			}
		});
		A52016.setIcon(new ImageIcon("F:\\eclipse-workspace\\Router-PROJECT\\Router-SLIKE\\A52016sumsung.jpg"));
		A52016.setBounds(693, 118, 76, 162);
		frame.getContentPane().add(A52016);



		JButton btnPCWifi = new JButton("Wifi");
		btnPCWifi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(checkBoxWifi.isSelected()==true) {
					passwordFieldWifi1.setVisible(true);
					btnPCWifi.setText("Connect");
					labelWifi1.setVisible(true);
					labelWifi1.setText("Please enter the security key");

					if(passwordFieldWifi1.getText().length()>0 && passwordFieldWifi1.getText()!=null){
						if(passwordFieldWifi1.getText().equals(sifra) || passwordFieldWifi1.getText().equals("test")) {
							linija1.setForeground(Color.GREEN);
							JOptionPane.showMessageDialog(null, "Uspesna konekcija!");
							btnPCWifi.setText("Connected!");
							passwordFieldWifi1.setVisible(false);
							labelWifi1.setVisible(false);


							Uredjaj u = new Uredjaj("HP-880", "wifi");

							JSetIp drugi = new JSetIp(u.getNaziv(), "Wifi");
							drugi.main(null);



							btnPCWifi.setEnabled(false);








						}
						else {
							JOptionPane.showMessageDialog(null, "Pogresna sifra!");
							passwordFieldWifi2.setText("");
						}
					}
					else {
						//JOptionPane.showMessageDialog(null, "Morate popuniti sva polja!");
					}

				}


				else {
					JOptionPane.showMessageDialog(null, "Nema dostupnih Wifi!");
				}




			}
		});
		btnPCWifi.setForeground(Color.BLUE);
		btnPCWifi.setIcon(new ImageIcon("F:\\eclipse-workspace\\Router-PROJECT\\Router-SLIKE\\wifi-icon.png"));
		btnPCWifi.setBounds(14, 19, 136, 34);
		frame.getContentPane().add(btnPCWifi);

		JButton btnPCEthernet = new JButton("Ethernet");
		btnPCEthernet.setForeground(Color.ORANGE);
		btnPCEthernet.setIcon(new ImageIcon("F:\\eclipse-workspace\\Router-PROJECT\\Router-SLIKE\\ethernet.png"));
		btnPCEthernet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				linija1.setForeground(Color.GREEN);


				JSetIp prvi = new JSetIp("HP-880", "Ethernet");
				prvi.main(null);

				btnPCEthernet.setEnabled(false);

			}
		});
		btnPCEthernet.setBounds(15, 164, 126, 34);
		frame.getContentPane().add(btnPCEthernet);

		JLabel lblS = new JLabel("Set a new password>>");
		lblS.setFont(new Font("Tahoma", Font.ITALIC, 12));
		lblS.setForeground(Color.BLUE);
		lblS.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				panel2.setVisible(true);
				separator_1.setVisible(false);
				lblNewPassword.setVisible(false);
				lblRepeatPassword.setVisible(false);
				passwordField_1.setVisible(false);
				passwordField_2.setVisible(false);
				btnEnter.setVisible(true);
				btnChangePassword.setVisible(false);

			}
		});
		lblS.setBounds(588, 314, 163, 16);
		frame.getContentPane().add(lblS);

		labelWifi1 = new JLabel("Enter key:");
		labelWifi1.setBounds(16, 0, 161, 16);
		frame.getContentPane().add(labelWifi1);

		labelIspisIp = new JLabel("");
		labelIspisIp.setBounds(200, 321, 376, 16);
		frame.getContentPane().add(labelIspisIp);

		passwordFieldWifi1 = new JPasswordField();
		passwordFieldWifi1.setBounds(172, 25, 102, 22);
		frame.getContentPane().add(passwordFieldWifi1);
		passwordFieldWifi1.setVisible(false);

		btnNewButton = new JButton("SHOW ");
		btnNewButton.setBackground(Color.BLUE);
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText("");
				textArea.setVisible(true);
				textArea.setForeground(Color.GREEN);
				listaUredjaja = vratiListuIzFajla("Adrese.txt");
				for(Uredjaj u: listaUredjaja) {
					textArea.append(u.toString()+"\n");
				}
			}
		});
		btnNewButton.setBounds(14, 532, 127, 25);
		frame.getContentPane().add(btnNewButton);

		linija3 = new JLabel("------------");
		linija3.setFont(new Font("Tahoma", Font.BOLD, 31));
		linija3.setForeground(Color.RED);
		linija3.setBounds(520, 208, 167, 16);
		frame.getContentPane().add(linija3);

		passwordFieldWifi3 = new JPasswordField();
		passwordFieldWifi3.setBounds(520, 87, 102, 25);
		frame.getContentPane().add(passwordFieldWifi3);
		passwordFieldWifi3.setVisible(false);

		JButton btnA5Wifi = new JButton("Wifi");
		btnA5Wifi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(checkBoxWifi.isSelected()==true) {
					passwordFieldWifi3.setVisible(true);
					btnA5Wifi.setText("Connect");
					labelWifi3.setVisible(true);
					labelWifi3.setText("Please enter the security key");

					if(passwordFieldWifi3.getText().length()>0 && passwordFieldWifi3.getText()!=null){
						if(passwordFieldWifi3.getText().equals(sifra) || passwordFieldWifi3.getText().equals("test")) {
							linija3.setForeground(Color.GREEN);
							JOptionPane.showMessageDialog(null, "Uspesna konekcija!");
							btnA5Wifi.setText("Connected!");
							passwordFieldWifi3.setVisible(false);
							labelWifi3.setVisible(false);


							Uredjaj u = new Uredjaj("Symsung galaxy A52016", "wifi");

							JSetIp drugi = new JSetIp(u.getNaziv(), "Wifi");
							drugi.main(null);



							btnA5Wifi.setEnabled(false);








						}
						else {
							JOptionPane.showMessageDialog(null, "Pogresna sifra!");
							passwordFieldWifi2.setText("");
						}
					}
					else {
						//JOptionPane.showMessageDialog(null, "Morate popuniti sva polja!");
					}

				}


				else {
					JOptionPane.showMessageDialog(null, "Nema dostupnih Wifi!");
				}




			}

		});
		btnA5Wifi.setIcon(new ImageIcon("F:\\eclipse-workspace\\Router-PROJECT\\Router-SLIKE\\wifi-icon.png"));
		btnA5Wifi.setForeground(Color.BLUE);
		btnA5Wifi.setBounds(640, 80, 136, 38);
		frame.getContentPane().add(btnA5Wifi);

		labelWifi3 = new JLabel("                         Enter key:");
		labelWifi3.setBounds(588, 60, 184, 16);
		frame.getContentPane().add(labelWifi3);







	}



	public  ArrayList<Uredjaj> vratiListuIzFajla(String datoteka){
		ArrayList<Uredjaj> baza = new ArrayList<Uredjaj>();


		Scanner sc=null;

		try {
			sc = new Scanner(new File(datoteka));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.exit(1);
		}


		String red;
		String[] art;
		while(sc.hasNext()){
			red = sc.nextLine(); 


			// UREDJAJ: 	naziv,nacin,ip

			art = red.split(",");

			Uredjaj u;


			int ip;
			try {
				ip=Integer.parseInt(art[3]);
				u = new Uredjaj(art[0], art[1],art[2], ip);

				baza.add(u);

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Pogresan format fajla");

				System.exit(2);		
			}
		}

		sc.close(); 
		return baza;



	}


	@Override
	public String probaCitanja(String datoteka) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void probaBrisanja(String datoteka) {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(datoteka);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		writer.print("");
		writer.close();
	}


	@Override
	public void probaPisanja(String datoteka, String pokazivac) {
		PrintWriter pw = null;

		try {
			pw = new PrintWriter(new FileWriter("Pokazivac.txt",true));

			pw.println(pokazivac);

		} catch (IOException e1) {
			e1.printStackTrace();
			System.out.println("Greska pri otvaranju fajla");
		} finally{
			pw.flush(); 			
			pw.close();
		}

	}
}
