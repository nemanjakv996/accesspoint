package program;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.CloseAction;
import javax.swing.text.html.HTMLEditorKit.Parser;

import model.IFajlovi;
import model.JTextFieldLimit;
import model.Uredjaj;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.print.attribute.IntegerSyntax;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.MalformedInputException;
import java.util.ArrayList;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;



public class JSetIp extends JIp implements IFajlovi {

	ArrayList<Uredjaj> lista = new ArrayList<>();

	private JFrame frame;

	private JTextField RtextField1;
	private JTextField RtextField2;
	private JTextField RtextField3;
	private JTextField RtextField4;
	private JTextField RtextField5;
	private JTextField RtextField6;
	private JTextField RtextField7;
	private JTextField RtextField8;
	private JTextField RtextField9;
	private JTextField RtextField10;
	private JTextField RtextField11;
	private JTextField RtextField12;
	private JTextField RtextField13;
	private JTextField RtextField14;
	private JTextField RtextField15;
	private JTextField RtextField16;

	private static String Naziv;  //potrebno za construktor koji prosledjuje elemente za kreiranje novog jframe-a
	private static String Nacin;

	private String pokazivac; //za smestanje pokazivaca u FILE




	public JSetIp() {
		initialize();
	}



	public JSetIp(String naziv, String nacin) {

		Naziv = naziv;
		Nacin = nacin;
		initialize();
	}




	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JSetIp window = new JSetIp(Naziv, Nacin);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */


	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();

		JRadioButton radioBtn1 = new JRadioButton("Static");
		JRadioButton radioBtn2 = new JRadioButton("DHCP");
			
		
		JLabel labelNaziv = new JLabel("");
		JLabel labelNacin = new JLabel("");


		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				pokazivac = probaCitanja("Pokazivac.txt");
				radioBtn2.setSelected(true);
				setDhcpFields();


			}

			@Override
			public void windowClosed(WindowEvent e) {
				String prvi;
				String drugi;
				String treci;
				String cetvrti;
				
			
					


				if(radioBtn1.isSelected()==true ) {
					cetvrti=RtextField4.getText();

					String red =labelNaziv.getText()+","+labelNacin.getText()+","+"192.168.0"+","+cetvrti;

					probaPisanjaStatic("Adrese.txt", red);

					//TEST
					JOptionPane.showMessageDialog(null,labelNaziv.getText()+","+labelNacin.getText()+","+"192.168.0"+","+cetvrti);


					frame.dispose();
				}

				else



					if(radioBtn2.isSelected()==true ) {
						cetvrti=pokazivac;

						String red =labelNaziv.getText()+","+labelNacin.getText()+","+"192.168.0"+","+cetvrti;

						probaPisanja("Adrese.txt", red);

						//TEST
						JOptionPane.showMessageDialog(null,labelNaziv.getText()+","+labelNacin.getText()+","+"192.168.0"+","+cetvrti);


						try {
							double broj= Double.parseDouble(cetvrti);
							broj=broj+1;

							int br = (int) broj;
							probaBrisanja("Pokazivac.txt");
							probaPisanja("Pokazivac.txt",""+br);

						} catch (Exception e2) {
							JOptionPane.showMessageDialog(null, "Nije moguce parsiranje");
						}
						frame.dispose();
					}
				
				
			}
			@Override
			public void windowClosing(WindowEvent arg0) {
				if(radioBtn1.isSelected()==true) {
					radioBtn1.setSelected(false);
					radioBtn2.setSelected(true);
				}
				if(radioBtn1.isSelected()==false || radioBtn2.isSelected()==false) {
					radioBtn2.setSelected(true);
				
				}
			}
		});
		frame.setBounds(100, 100, 332, 405);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 92, 324, 272);
		panel.setBorder(new TitledBorder(null, "Set Ip configuration", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		frame.getContentPane().add(panel);
		panel.setLayout(null);





		//Radio BUtton STATIC
		radioBtn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(radioBtn1.isSelected()==true) {
					radioBtn1.setSelected(true);
					radioBtn2.setSelected(false);

					setStaticFields();

				}

			}
		});
		radioBtn1.setBounds(23, 39, 91, 25);
		panel.add(radioBtn1);

		//Radio BUtton DHCP
		radioBtn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(radioBtn2.isSelected()==true) {
					radioBtn2.setSelected(true);
					radioBtn1.setSelected(false);

					setDhcpFields();


				}
			}
		});
		radioBtn2.setBounds(143, 39, 91, 25);
		panel.add(radioBtn2);

		JLabel label1 = new JLabel("Ip Address:");
		label1.setBounds(23, 96, 66, 16);
		panel.add(label1);

		JLabel lblNewLabel_1 = new JLabel("Subnet Mask:");
		lblNewLabel_1.setBounds(23, 132, 78, 16);
		panel.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Default Gateway:");
		lblNewLabel_2.setBounds(23, 179, 98, 16);
		panel.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("DNS Server:");
		lblNewLabel_3.setBounds(23, 214, 78, 16);
		panel.add(lblNewLabel_3);

		RtextField1 = new JTextField();
		RtextField1.setBounds(122, 90, 36, 22);
		panel.add(RtextField1);
		RtextField1.setColumns(10);

		JLabel lblNewLabel = new JLabel(".");
		lblNewLabel.setBounds(162, 96, 4, 16);
		panel.add(lblNewLabel);

		RtextField2 = new JTextField();
		RtextField2.setColumns(10);
		RtextField2.setBounds(170, 90, 36, 22);
		panel.add(RtextField2);

		JLabel label_1 = new JLabel(".");
		label_1.setBounds(208, 96, 4, 16);
		panel.add(label_1);

		RtextField3 = new JTextField();
		RtextField3.setColumns(10);
		RtextField3.setBounds(218, 90, 36, 22);
		panel.add(RtextField3);

		RtextField4 = new JTextField();
		RtextField4.setColumns(10);
		RtextField4.setBounds(264, 90, 36, 22);
		panel.add(RtextField4);

		JLabel label = new JLabel(".");
		label.setBounds(256, 96, 4, 16);
		panel.add(label);

		RtextField5 = new JTextField();
		RtextField5.setColumns(10);
		RtextField5.setBounds(122, 126, 36, 22);
		panel.add(RtextField5);

		JLabel label_2 = new JLabel(".");
		label_2.setBounds(162, 132, 4, 16);
		panel.add(label_2);

		RtextField6 = new JTextField();
		RtextField6.setColumns(10);
		RtextField6.setBounds(170, 126, 36, 22);
		panel.add(RtextField6);

		JLabel label_3 = new JLabel(".");
		label_3.setBounds(208, 132, 4, 16);
		panel.add(label_3);

		RtextField7 = new JTextField();
		RtextField7.setColumns(10);
		RtextField7.setBounds(218, 126, 36, 22);
		panel.add(RtextField7);

		JLabel label_4 = new JLabel(".");
		label_4.setBounds(256, 132, 4, 16);
		panel.add(label_4);

		RtextField8 = new JTextField();
		RtextField8.setColumns(10);
		RtextField8.setBounds(264, 126, 36, 22);
		panel.add(RtextField8);

		RtextField9 = new JTextField();
		RtextField9.setColumns(10);
		RtextField9.setBounds(122, 173, 36, 22);
		panel.add(RtextField9);

		JLabel label_5 = new JLabel(".");
		label_5.setBounds(162, 179, 4, 16);
		panel.add(label_5);

		RtextField10 = new JTextField();
		RtextField10.setColumns(10);
		RtextField10.setBounds(170, 173, 36, 22);
		panel.add(RtextField10);

		JLabel label_6 = new JLabel(".");
		label_6.setBounds(208, 179, 4, 16);
		panel.add(label_6);

		RtextField11 = new JTextField();
		RtextField11.setColumns(10);
		RtextField11.setBounds(218, 173, 36, 22);
		panel.add(RtextField11);

		JLabel label_7 = new JLabel(".");
		label_7.setBounds(256, 179, 4, 16);
		panel.add(label_7);

		RtextField12 = new JTextField();
		RtextField12.setColumns(10);
		RtextField12.setBounds(264, 173, 36, 22);
		panel.add(RtextField12);

		RtextField13 = new JTextField();
		RtextField13.setColumns(10);
		RtextField13.setBounds(122, 208, 36, 22);
		panel.add(RtextField13);

		JLabel label_8 = new JLabel(".");
		label_8.setBounds(162, 214, 4, 16);
		panel.add(label_8);

		RtextField14 = new JTextField();
		RtextField14.setColumns(10);
		RtextField14.setBounds(170, 208, 36, 22);
		panel.add(RtextField14);

		JLabel label_9 = new JLabel(".");
		label_9.setBounds(208, 214, 4, 16);
		panel.add(label_9);

		RtextField15 = new JTextField();
		RtextField15.setColumns(10);
		RtextField15.setBounds(218, 208, 36, 22);
		panel.add(RtextField15);

		JLabel label_10 = new JLabel(".");
		label_10.setBounds(256, 214, 4, 16);
		panel.add(label_10);

		RtextField16 = new JTextField();
		RtextField16.setColumns(10);
		RtextField16.setBounds(264, 208, 36, 22);
		panel.add(RtextField16);

		JSeparator separator = new JSeparator();
		separator.setBounds(33, 161, 267, 2);
		panel.add(separator);







		JButton btnDisable = new JButton("save");
		btnDisable.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) { 
				String prvi;
				String drugi;
				String treci;
				String cetvrti;

				if(radioBtn2.isSelected()==true ){
					/*prvi=RtextField1.getText();
					drugi=RtextField2.getText();
					treci=RtextField3.getText();

					cetvrti=pokazivac;

					String red =labelNaziv.getText()+","+labelNacin.getText()+","+ prvi+"."+drugi+"."+treci+","+cetvrti;

					probaPisanja("Adrese.txt", red);


					//TEST
					JOptionPane.showMessageDialog(null,labelNaziv.getText()+","+labelNacin.getText()+","+ prvi+"."+drugi+"."+treci+","+cetvrti);


					try {
						double broj= Double.parseDouble(cetvrti);
						broj=broj+1;

						int br = (int) broj;
						probaBrisanja("Pokazivac.txt");
						probaPisanja("Pokazivac.txt",""+br);

					} catch (Exception e2) {
						JOptionPane.showMessageDialog(null, "Nije moguce parsiranje");
					}*/


					frame.dispose();

				}
				else
					if(radioBtn1.isSelected()==true) {

						prvi=RtextField1.getText();
						drugi=RtextField2.getText();
						treci=RtextField3.getText();
						cetvrti=RtextField4.getText();

						if(prvi.length()>0 && prvi!=null && drugi.length()>0 && drugi!=null && treci.length()>0 && treci!=null && cetvrti.length()>0 && cetvrti!=null) {					
							try {

								int pr = Integer.parseInt(prvi);
								int dr = Integer.parseInt(drugi);
								int tr = Integer.parseInt(treci);
								int ce =Integer.parseInt(cetvrti);


								if( pr==192 && dr==168 &&  tr==0 && ce>1 && ce<= 100 ) {



									if(postojiAdresa(ce)==true) {


										/*String red =labelNaziv.getText()+","+labelNacin.getText()+","+ prvi+"."+drugi+"."+treci+","+cetvrti+"\n";

										probaPisanja("Adrese.txt", red);*/




										frame.dispose();

									}else {
										JOptionPane.showMessageDialog(null, "Zauzeta adresa");
										clearFields();
									}


								}else {
									JOptionPane.showMessageDialog(null, "Uneta adresa pripada opsegu DHCP servera\nPrimer IP adrese [192.168.0.2-100]");
									clearFields();
								}

							} catch (Exception e2) {
								JOptionPane.showMessageDialog(null, "IP adresa mora biti broj!");	
								clearFields();

							}
						}else {
							JOptionPane.showMessageDialog(null, "Morate popuniti sva polja!");
							clearFields();
						}
					}

			}

		});
		btnDisable.setBackground(Color.BLUE);
		btnDisable.setForeground(Color.WHITE);
		btnDisable.setBounds(208, 234, 97, 25);
		panel.add(btnDisable);



		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Natpisi", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(0, 0, 314, 93);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);



		labelNaziv.setBounds(46, 36, 56, 16);//label naziv
		panel_1.add(labelNaziv);


		labelNacin.setBounds(182, 36, 56, 16);
		panel_1.add(labelNacin);

		labelNaziv.setText(Naziv);
		labelNacin.setText(Nacin);



	}

	public void clearFields() {
		RtextField1.setText("");
		//RtextField1.setBackground(Color.WHITE);
		RtextField2.setText("");
		//RtextField2.setBackground(Color.WHITE);
		RtextField3.setText("");
		//RtextField3.setBackground(Color.WHITE);
		RtextField4.setText("");
		//RtextField4.setBackground(Color.WHITE);

	}

	public  String probaCitanja(String datoteka) {
		Scanner sc=null;
		String red="";
		try {
			sc=new Scanner(new File(datoteka));
			red="";
			while(sc.hasNext()) {
				red+=sc.nextLine()+"\n";
			}



		}catch (FileNotFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println("Greska prilikom otvaranja File-a");
		}finally {
			sc.close();
		}
		return red;
	}

	public void probaBrisanja(String datoteka) {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(datoteka);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		writer.print("");
		writer.close();

	}

	public void probaPisanja(String datoteka, String pokazivac) {
		PrintWriter pw = null;

		try {
			pw = new PrintWriter(new FileWriter(datoteka,true));

			pw.print(pokazivac);

		} catch (IOException e1) {
			e1.printStackTrace();
			System.out.println("Greska pri otvaranju fajla");
		} finally{
			pw.flush(); 			// isprazni buffer
			pw.close();
		}

	}

	public void probaPisanjaStatic(String datoteka, String pokazivac) {
		PrintWriter pw = null;

		try {
			pw = new PrintWriter(new FileWriter(datoteka,true));

			pw.print(pokazivac+"\n");

		} catch (IOException e1) {
			e1.printStackTrace();
			System.out.println("Greska pri otvaranju fajla");
		} finally{
			pw.flush(); 			// isprazni buffer
			pw.close();
		}

	}


	private boolean postojiAdresa(int ce) {
		lista = vratiListuIzFajla("Adrese.txt");

		for(Uredjaj u: lista) 
			if(u.getPokazivac()==ce) 
				return false;

		return true;
	}

	public  ArrayList<Uredjaj> vratiListuIzFajla(String datoteka){
		ArrayList<Uredjaj> baza = new ArrayList<Uredjaj>();


		Scanner sc=null;

		try {
			sc = new Scanner(new File(datoteka));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.exit(1);
		}


		String red;
		String[] art;
		while(sc.hasNext()){
			red = sc.nextLine(); 	

			// UREDJAJ: 	naziv,nacin,ip

			art = red.split(",");

			Uredjaj u;

			int ip;
			try {
				ip=Integer.parseInt(art[3]);
				u = new Uredjaj(art[0], art[1],art[2], ip);

				baza.add(u);

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Pogresan format fajla");

				System.exit(2);		
			}
		}

		sc.close(); 
		return baza;



	}

	private void setStaticFields() {
		RtextField1.setText("");
		RtextField1.setEnabled(true);
		RtextField1.setDocument(new JTextFieldLimit(3));//ogranicenje polja na max 3 karaktera 

		RtextField2.setText("");
		RtextField2.setEnabled(true);
		RtextField2.setDocument(new JTextFieldLimit(3));

		RtextField3.setText("");
		RtextField3.setEnabled(true);
		RtextField3.setDocument(new JTextFieldLimit(3));

		RtextField4.setText("");
		RtextField4.setEnabled(true);
		RtextField4.setDocument(new JTextFieldLimit(3));


		//SUbnet Mask
		RtextField5.setText("255");
		RtextField5.setEnabled(false);
		RtextField5.setBackground(Color.DARK_GRAY);
		RtextField5.setForeground(Color.WHITE);

		RtextField6.setText("255");
		RtextField6.setEnabled(false);
		RtextField6.setBackground(Color.DARK_GRAY);
		RtextField6.setForeground(Color.WHITE);

		RtextField7.setText("255");
		RtextField7.setEnabled(false);
		RtextField7.setBackground(Color.DARK_GRAY);
		RtextField7.setForeground(Color.WHITE);

		RtextField8.setText("0");
		RtextField8.setEnabled(false);
		RtextField8.setBackground(Color.DARK_GRAY);
		RtextField8.setForeground(Color.WHITE);

		//Default gateway
		RtextField9.setText("192");
		RtextField9.setEnabled(false);
		RtextField9.setBackground(Color.GRAY);
		RtextField9.setForeground(Color.WHITE);

		RtextField10.setText("168");
		RtextField10.setEnabled(false);
		RtextField10.setBackground(Color.GRAY);
		RtextField10.setForeground(Color.WHITE);

		RtextField11.setText("0");
		RtextField11.setEnabled(false);
		RtextField11.setBackground(Color.GRAY);
		RtextField11.setForeground(Color.WHITE);

		RtextField12.setText("1");
		RtextField12.setEnabled(false);
		RtextField12.setBackground(Color.GRAY);
		RtextField12.setForeground(Color.WHITE);

		//DNS Server
		RtextField13.setText("188");
		RtextField13.setEnabled(false);
		RtextField13.setBackground(Color.GRAY);
		RtextField13.setForeground(Color.WHITE);

		RtextField14.setText("255");
		RtextField14.setEnabled(false);
		RtextField14.setBackground(Color.GRAY);
		RtextField14.setForeground(Color.WHITE);

		RtextField15.setText("196");
		RtextField15.setEnabled(false);
		RtextField15.setBackground(Color.GRAY);
		RtextField15.setForeground(Color.WHITE);

		RtextField16.setText("222");
		RtextField16.setEnabled(false);
		RtextField16.setBackground(Color.GRAY);
		RtextField16.setForeground(Color.WHITE);


	}

	private void setDhcpFields() {
		//router IP ADRESS



		RtextField1.setText("192");
		RtextField1.setEnabled(false);
		RtextField2.setText("168");
		RtextField2.setEnabled(false);
		RtextField3.setText("0");
		RtextField3.setEnabled(false);
		RtextField4.setText(pokazivac);
		RtextField4.setEnabled(false);


		//SUbnet Mask
		RtextField5.setText("255");
		RtextField5.setEnabled(false);
		RtextField5.setBackground(Color.GRAY);
		RtextField5.setForeground(Color.WHITE);

		RtextField6.setText("255");
		RtextField6.setEnabled(false);
		RtextField6.setBackground(Color.GRAY);
		RtextField6.setForeground(Color.WHITE);

		RtextField7.setText("255");
		RtextField7.setEnabled(false);
		RtextField7.setBackground(Color.GRAY);
		RtextField7.setForeground(Color.WHITE);

		RtextField8.setText("0");
		RtextField8.setEnabled(false);
		RtextField8.setBackground(Color.GRAY);
		RtextField8.setForeground(Color.WHITE);



		//Default gateway
		RtextField9.setText("192");
		RtextField9.setEnabled(false);
		RtextField9.setBackground(Color.GRAY);
		RtextField9.setForeground(Color.WHITE);

		RtextField10.setText("168");
		RtextField10.setEnabled(false);
		RtextField10.setBackground(Color.GRAY);
		RtextField10.setForeground(Color.WHITE);

		RtextField11.setText("0");
		RtextField11.setEnabled(false);
		RtextField11.setBackground(Color.GRAY);
		RtextField11.setForeground(Color.WHITE);

		RtextField12.setText("1");
		RtextField12.setEnabled(false);
		RtextField12.setBackground(Color.GRAY);
		RtextField12.setForeground(Color.WHITE);

		//DNS Server
		RtextField13.setText("188");
		RtextField13.setEnabled(false);
		RtextField13.setBackground(Color.GRAY);
		RtextField13.setForeground(Color.WHITE);

		RtextField14.setText("255");
		RtextField14.setEnabled(false);
		RtextField14.setBackground(Color.GRAY);
		RtextField14.setForeground(Color.WHITE);

		RtextField15.setText("196");
		RtextField15.setEnabled(false);
		RtextField15.setBackground(Color.GRAY);
		RtextField15.setForeground(Color.WHITE);

		RtextField16.setText("222");
		RtextField16.setEnabled(false);
		RtextField16.setBackground(Color.GRAY);
		RtextField16.setForeground(Color.WHITE);

	}
}
