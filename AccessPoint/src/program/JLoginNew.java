package program;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JTextField;


import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class JLoginNew {

	private JFrame frame;
	private JTextField textFieldUserName;
	private JPasswordField textFieldPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JLoginNew window = new JLoginNew();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JLoginNew() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 452, 366);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel panel1 = new JPanel();
		panel1.setBackground(new Color(255, 0, 0));
		panel1.setBounds(0, 0, 434, 122);
		frame.getContentPane().add(panel1);
		panel1.setLayout(null);


		
		JLabel lblNewLabel = new JLabel();
		frame.add(lblNewLabel);
		lblNewLabel.setBounds(0, 0, 127, 122);
		panel1.add(lblNewLabel);
		
		
		
		
		lblNewLabel.setIcon(new ImageIcon("F:\\eclipse-workspace\\Router-PROJECT\\Router-SLIKE\\FTNlogo.png"));

		JLabel label = new JLabel("\u0424\u0410\u041A\u0423\u041B\u0422\u0415\u0422 \u0422\u0415\u0425\u041D\u0418\u0427\u041A\u0418\u0425 \u041D\u0410\u0423\u041A\u0410 \u0423 \u0427\u0410\u0427\u041A\u0423");
		label.setBounds(145, 53, 254, 16);
		panel1.add(label);
		label.setFont(new Font("Tahoma", Font.BOLD, 12));
		label.setForeground(Color.WHITE);

		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(143, 71, 254, 2);
		panel1.add(separator_2);

		JPanel panel2 = new JPanel();
		panel2.setForeground(Color.WHITE);
		panel2.setBackground(Color.GRAY);
		panel2.setBounds(0, 121, 434, 198);
		frame.getContentPane().add(panel2);
		panel2.setLayout(null);

		textFieldUserName = new JTextField();
		textFieldUserName.setBounds(158, 44, 116, 22);
		panel2.add(textFieldUserName);
		textFieldUserName.setColumns(10);

		JLabel lblUserName = new JLabel("User_name:");
		lblUserName.setBounds(41, 45, 149, 16);
		panel2.add(lblUserName);
		lblUserName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblUserName.setForeground(Color.WHITE);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(41, 87, 81, 16);
		panel2.add(lblPassword);
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPassword.setForeground(Color.WHITE);

		JButton btnLogin = new JButton("Login:");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String userName = textFieldUserName.getText().trim();
				String password = textFieldPassword.getText().trim();

				if(userName!= null && userName.length()>0 && password!=null &&password.length()>0) {
					if(userName.equals("admin") && password.equals("admin") ) {
						//JOptionPane.showMessageDialog(null, "Uspesno!");
						textFieldUserName.setText(null);
						textFieldPassword.setText(null);

						JIp pr= new JIp();
						pr.main(null);

						frame.dispose();

					}else {
						JOptionPane.showMessageDialog(null, "Pogresno uneto User_name ili Password!","GRESKA!",JOptionPane.ERROR_MESSAGE);
						textFieldUserName.setText(null);
						textFieldPassword.setText(null);
					}

				}else {
					JOptionPane.showMessageDialog(null, "Morate popuniti sva polja!");

				}





			}

		});
		btnLogin.setBounds(41, 132, 97, 25);
		panel2.add(btnLogin);
		btnLogin.setForeground(new Color(0, 0, 0));
		btnLogin.setBackground(Color.LIGHT_GRAY);
		btnLogin.setFont(new Font("Tahoma", Font.BOLD, 18));
		
		
		

		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldUserName.setText(null);
				textFieldPassword.setText(null);
			}
		});
		btnReset.setBackground(Color.LIGHT_GRAY);
		btnReset.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnReset.setBounds(168, 132, 97, 25);
		panel2.add(btnReset);

		textFieldPassword = new JPasswordField();
		textFieldPassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER) {
					String userName = textFieldUserName.getText().trim();
					String password = textFieldPassword.getText().trim();

					if(userName!= null && userName.length()>0 && password!=null &&password.length()>0) {
						if(userName.equals("admin") && password.equals("admin") ) {
							//JOptionPane.showMessageDialog(null, "Uspesno!");
							textFieldUserName.setText(null);
							textFieldPassword.setText(null);

							JIp pr= new JIp();
							pr.main(null);

							frame.dispose();

						}else {
							JOptionPane.showMessageDialog(null, "Pogresno uneto User_name ili Password!","GRESKA!",JOptionPane.ERROR_MESSAGE);
							textFieldUserName.setText(null);
							textFieldPassword.setText(null);
						}

					}else {
						JOptionPane.showMessageDialog(null, "Morate popuniti sva polja!");

					}
					
					
				}
			}
		});
		textFieldPassword.setBounds(158, 86, 116, 22);
		panel2.add(textFieldPassword);

		JSeparator separator = new JSeparator();
		separator.setBounds(31, 116, 264, 3);
		panel2.add(separator);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(31, 29, 264, 3);
		panel2.add(separator_1);

		JLabel lblClickHereTo = new JLabel("click here to registration");
		lblClickHereTo.setToolTipText("");
		lblClickHereTo.setBounds(98, 169, 166, 16);
		panel2.add(lblClickHereTo);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
			}
		});
		btnExit.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnExit.setBackground(Color.LIGHT_GRAY);
		btnExit.setBounds(292, 134, 97, 25);
		panel2.add(btnExit);
	}
}
