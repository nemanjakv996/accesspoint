package model;

public class Router {
	protected String naziv;
	protected String ipAddress;
	protected String subNet;
	

	public Router(String naziv, String ipAddress, String subNet) {
		super();
		this.naziv = naziv;
		this.ipAddress = ipAddress;
		this.subNet = subNet;
	}



	@Override
	public String toString() {
		return "Router:" + naziv + "," + ipAddress + "," + subNet ;
	}


	
	
	



}
