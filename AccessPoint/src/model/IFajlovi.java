package model;

import java.util.ArrayList;

public interface IFajlovi {
	public  String probaCitanja(String datoteka);
	public void probaBrisanja(String datoteka);
	public void probaPisanja(String datoteka, String pokazivac);
	public  ArrayList<Uredjaj> vratiListuIzFajla(String datoteka);

}
