package model;

import java.security.KeyStore.Entry.Attribute;

import javax.print.attribute.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

@SuppressWarnings("serial")
public class JTextFieldLimit extends PlainDocument {
	
	private int limit;
	
	public JTextFieldLimit(int unesiGranicu	) {
		this.limit=unesiGranicu;
	}

	public void insertString(int offset, String str, javax.swing.text.AttributeSet set) throws BadLocationException		{
		if(str == null) {
			return;
		}
		else 
			if((getLength() + str.length())<= limit) {
				str=str.toUpperCase();
				super.insertString(offset, str, set);
			}
		
			
		
	}
		
	
}
